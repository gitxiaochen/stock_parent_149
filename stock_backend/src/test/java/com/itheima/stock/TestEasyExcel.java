package com.itheima.stock;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.itheima.stock.pojo.User;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author by itheima
 * @Date
 * @Description 测试数据导出
 */
public class TestEasyExcel {
    /**
     * 模拟从数据库查询到的数据
     *
     * @return
     */
    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            User u = User.builder().userName("zhangsan_" + i).birthday(new Date()).address("sh_" + i).age(5 + i).build();
            users.add(u);
        }
        return users;
    }

    /**
     * @desc 测试数据导出
     */
    @Test
    public void testExportData() {
        List<User> users = getUsers();
        EasyExcel
                .write("D:\\桌面\\excel\\user.xls", User.class)
                .sheet("用户信息列表")
                .doWrite(users);
    }

    /**
     * @desc 测试导入数据
     */
    @Test
    public void testImportDate() {
        String filePath = "D:\\桌面\\excel\\user.xls";
        List<User> users = new ArrayList<>();
        EasyExcel
                .read(filePath, User.class, new AnalysisEventListener<User>() {
                    /**
                     * 解析器解析每行的数据并封装成User类型的对象,然后注入到该方法下
                     * @param data
                     * @param context
                     */
                    @Override
                    public void invoke(User data, AnalysisContext context) {
                        users.add(data);
                    }

                    /**
                     * excel所有行解析完毕后的触发动作
                     * @param context
                     */
                    @Override
                    public void doAfterAllAnalysed(AnalysisContext context) {
                        System.out.println("excel文件已经读取完毕.....");
                    }
                })
                .sheet("用户信息列表")
                .doRead();
    }
}