package com.itheima.stock.web;

import com.itheima.stock.pojo.entity.SysUser;
import com.itheima.stock.service.UserService;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/*
 * 定义访问用户的服务接口
 * */
@RestController
@RequestMapping("/api")
@Api(tags = "用户操作的接口")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @ApiOperation("根据用户名获取用户信息的接口")
    //@ApiImplicitParam()//单个参数
    @ApiImplicitParams({//多个参数
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "path")
    })
    @GetMapping("/{userName}")
    public SysUser getUserByUserName(@PathVariable("userName") String userName) {
        return userService.getUserByUserName(userName);
    }

    /**
     * 用户登录功能
     *
     * @param reqVo
     * @return
     */
    @ApiOperation("用户登录功能接口")
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo reqVo) {
        return userService.login(reqVo);
    }

    /**
     * 获取校验码
     * @return
     */
    @ApiOperation("获取验证码接口")
    @GetMapping("/captcha")
    public R<Map> getCheckCode() {
        return userService.getCheckCode();
    }
}
