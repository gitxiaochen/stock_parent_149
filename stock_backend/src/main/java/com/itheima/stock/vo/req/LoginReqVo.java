package com.itheima.stock.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/*
* 登录时请求参数封装vo-value object view-object
* */
@Data
@ApiModel("用户登录时参数封装对象")
public class LoginReqVo {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;

    /**
     * 密码
     */
    @ApiModelProperty("明文密码")
    private String password;
    /**
     * 登录的校验码
     */
    @ApiModelProperty("随机校验码")
    private String code;
    /**
     * sessionId
     */
    @ApiModelProperty("会话ID")
    private String rkey;
}
