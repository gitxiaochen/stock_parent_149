package com.itheima.stock;

import com.itheima.stock.config.SwaggerConfiguration;
import com.itheima.stock.pojo.domain.StockInfoConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EnableConfigurationProperties(StockInfoConfig.class)
@MapperScan("com.itheima.stock.mapper")//扫描mapper接口,生成代理对象,并被ioc容器管理
@Import({SwaggerConfiguration.class})//将swagger配置类导入当前ioc容器中
public class StockApp {
    public static void main(String[] args) {
        SpringApplication.run(StockApp.class, args);
    }
}
