package com.itheima.stock.service.impl;

import com.itheima.stock.constant.StockConstant;
import com.itheima.stock.mapper.SysUserMapper;
import com.itheima.stock.pojo.entity.SysUser;
import com.itheima.stock.service.UserService;
import com.itheima.stock.utils.IdWorker;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.R;
import com.itheima.stock.vo.resp.ResponseCode;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @Override
    public SysUser getUserByUserName(String userName) {
        return sysUserMapper.getUserByUserName(userName);
    }

    /**
     * 用户登录功能
     *
     * @param reqVo
     * @return
     */
    @Override
    public R<LoginRespVo> login(LoginReqVo reqVo) {
        //判断输入参数的合法性
        if (reqVo == null || StringUtils.isBlank(reqVo.getUsername()) || StringUtils.isBlank(reqVo.getPassword())) {
            return R.error(ResponseCode.USERNAME_OR_PASSWORD_ERROR);
        }

        //判断校验码是否输入
        if (StringUtils.isBlank(reqVo.getCode()) || StringUtils.isBlank(reqVo.getRkey())) {
            return R.error(ResponseCode.CHECK_CODE_NOT_EMPTY);
        }

        //判断校验码是否一致
        String redisCheckCode = (String) redisTemplate.opsForValue().get(StockConstant.CHECK_PREFIX + reqVo.getRkey());
        if (!reqVo.getCode().equals(redisCheckCode)) {
            return R.error(ResponseCode.CHECK_CODE_ERROR);
        }

        //想快速淘汰缓存中的数据则自动调用删除校验码缓存操作
        //redisTemplate.delete("CK:" + reqVo.getRkey());

        //根据用户名查询用户信息
        SysUser dbUser = sysUserMapper.getUserByUserName(reqVo.getUsername());
        //判断用户是否存在
        if (dbUser == null || !passwordEncoder.matches(reqVo.getPassword(), dbUser.getPassword())) {
            return R.error(ResponseCode.USERNAME_OR_PASSWORD_ERROR);
        }
        //构建响应相对
        LoginRespVo respVo = new LoginRespVo();
//        respVo.setId(dbUser.getId());
//        respVo.setNickName(dbUser.getNickName());
        //我们发现respVo与dbUser下具有相同的属性,所以注解复制即可
        BeanUtils.copyProperties(dbUser, respVo);
        return R.ok(respVo);
    }

    /**
     * 获取校验码
     *
     * @return
     */
    @Override
    public R<Map> getCheckCode() {
        //1.生成sessionId  rkey  基于雪花算法生成的id比较长(19),如果直接以long类型响应给前端,会导致浏览器获取数据时丢失精度
        long sessionId = idWorker.nextId();
        String sessionStrId = String.valueOf(sessionId);
        //2.生成随机校验码  长度为4,纯数字
        String checkCode = RandomStringUtils.randomNumeric(4);
        //3.将校验码保存在session下 --> 保存在redis下
        redisTemplate.opsForValue().set(StockConstant.CHECK_PREFIX + sessionStrId, checkCode, 2, TimeUnit.MINUTES);
        //4.响应前端
        Map<String, String> info = new HashMap<>();
        info.put("code", checkCode);
        info.put("rkey", sessionStrId);
        return R.ok(info);

    }
}
