package com.itheima.stock.service;

import com.itheima.stock.pojo.entity.SysUser;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.R;

import java.util.Map;

/*
* 定义操纵用户的服务接口
* */
public interface UserService {
    /**
     * 根据账号名称查询用户信息
     * @param userName
     * @return
     */
    SysUser getUserByUserName(String userName);

    /**
     * 用户登录功能
     * @param reqVo
     * @return
     */
    R<LoginRespVo> login(LoginReqVo reqVo);

    /**
     * 获取校验码
     * @return
     */
    R<Map> getCheckCode();
}
